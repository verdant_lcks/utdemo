#include "moduleb.h"

STATIC moduleB_data gData;

void moduleB_init(void){
  moduleB_disable();
  moduleB_set(0);
}

void moduleB_set(uint8_t value){
  gData.value = value;
}

void moduleB_enable(void){
  gData.enabled = true;
}

void moduleB_disable(void){
  gData.enabled = false;
}

bool moduleB_isEnabled(void){
  return gData.enabled;
}

moduleB_data *moduleB_get(void){
  return &gData;
}
