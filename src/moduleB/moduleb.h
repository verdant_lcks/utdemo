#ifndef _MODULEB_H
#define _MODULEB_H

#include "defs.h"
#include "modulea.h"

typedef struct _moduleB_data {
  uint8_t value;
  bool    enabled;
}moduleB_data;

void moduleB_init(void);
void moduleB_set(uint8_t value);
void moduleB_enable(void);
void moduleB_disable(void);
bool moduleB_isEnabled(void);
moduleB_data *moduleB_get(void);

#endif // _MODULEB_H
