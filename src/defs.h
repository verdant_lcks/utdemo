#ifndef _DEFS_H_
#define _DEFS_H_

#include <stdint.h>
#include <stdbool.h>

#ifdef _TEST_
  #define STATIC
#else
  #define STATIC static
#endif

#ifndef NULL
  #define NULL (void*)0
#endif

#endif
