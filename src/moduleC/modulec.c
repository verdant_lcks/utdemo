#include "modulec.h"


bool modulec_run(void){
  bool ret = false;
  moduleB_data *ptr;
  if(moduleB_isEnabled()){
    ptr = moduleB_get();
    if(ptr) {
      if(ptr->value < 100) {
        moduleB_set(55);
      } else if(ptr->value < 200) {
        ret = true;
      } else {
        moduleB_disable();
      }
    }
  }
  return ret;
}
