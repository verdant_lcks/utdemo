#include "modulea.h"

STATIC uint8_t var_a;

void init_varA(void){
  var_a = 0;
}

void set_varA(uint8_t new) {
  var_a = new;
}

uint8_t get_varA(void){
  return var_a  ;
}
