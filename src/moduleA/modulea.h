#ifndef _MODULEA_H
#define _MODULEA_H

#include "defs.h"

void init_varA(void);
void set_varA(uint8_t new);
uint8_t get_varA(void);

#endif // _MODULEA_H
