#!/bin/bash -e

VM_NAME="dev_user"
if [ $# -gt 0 ]; then
  if [ $1 = "stop" ]; then
    VM_NAME="dev_$2"
  else
    VM_NAME="dev_$1"
  fi
fi

# Starting container
HOST=$(pwd)
DOCKER_RUN=$(docker ps -a | grep $VM_NAME) || echo "Docker Container is not started. Starting..."

if [ "$1" = "stop" ]; then
  docker stop $VM_NAME
  docker rm $VM_NAME
elif [ "$1" = "images" ]; then
  docker images
elif [ "$1" = "vars" ] && [ "$OS" == "Darwin" ]; then
  eval $(docker-machine env default)
elif [ -z "$DOCKER_RUN" ]; then
  OS=$(uname)
  if [ "$OS" == "Darwin" ]; then
    SSH="/Users/$USER"
  else
    SSH="/home/$USER"
  fi
  docker run -i -t -d \
             --name $VM_NAME \
             --privileged="true" \
             -v $SSH/.ssh:/home/$USER/.ssh \
             -v $HOST:/home/$USER/src \
             dev:user \
             /bin/bash
  sleep 1
  docker exec -it $VM_NAME /bin/bash
else
  docker exec -it $VM_NAME /bin/bash
fi
