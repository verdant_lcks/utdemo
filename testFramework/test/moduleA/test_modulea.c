#include "unity.h"
#include "modulea.h"

// variables
extern uint8_t var_a;

void setUp(void)
{
    init_varA();
}

void tearDown(void)
{
}

void test_modulea_init_varA(void)
{
    TEST_ASSERT_EQUAL(0, var_a);
}

void test_modulea_set_varA_5(void) {

  set_varA(5);
  TEST_ASSERT_EQUAL(5, var_a);
}

void test_modulea_set_varA_10(void) {

  set_varA(10);
  TEST_ASSERT_EQUAL(10, var_a);
}

void test_modulea_get_varA(void){
  set_varA(20);
  TEST_ASSERT_EQUAL(20, get_varA());
}
