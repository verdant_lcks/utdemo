#include "unity.h"
#include "moduleb.h"

extern moduleB_data gData;

void setUp(void)
{
  moduleB_init();
  TEST_ASSERT_FALSE(gData.enabled);
  TEST_ASSERT_FALSE(gData.value);
}

void tearDown(void)
{
}

void test_moduleb_isEnabled(void)
{
    TEST_ASSERT_FALSE(moduleB_isEnabled());
}

void test_moduleb_set(void){
  moduleB_data *ptr = moduleB_get();
  moduleB_set(50);
  TEST_ASSERT_EQUAL(50, ptr->value);
}

void test_moduleb_set_struct_inspection(void){
  moduleB_set(50);
  TEST_ASSERT_EQUAL(50, gData.value);
}

void test_moduleb_set_ptr_equal(void){
  moduleB_data *ptr = moduleB_get();
  moduleB_set(50);
  TEST_ASSERT_EQUAL_PTR(ptr, &gData);
}

void test_moduleb_enable(void){
  moduleB_enable();
  TEST_ASSERT_TRUE(moduleB_isEnabled());
}

void test_moduleb_enable_struct_inspection(void){
  moduleB_enable();
  TEST_ASSERT_TRUE(gData.enabled);
}

void test_moduleb_disable(void){
  moduleB_disable();
  TEST_ASSERT_FALSE(moduleB_isEnabled());
}

void test_moduleb_disable_struct_inspection(void){
  moduleB_disable();
  TEST_ASSERT_FALSE(gData.enabled);
}
