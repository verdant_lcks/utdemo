#include "unity.h"
#include "modulec.h"
#include "mock_moduleb.h"

void setUp(void)
{
}

void tearDown(void)
{
}

void test_modulec_run_moduleb_disabled(void)
{
    moduleB_isEnabled_ExpectAndReturn(false);
    TEST_ASSERT_FALSE(modulec_run());
}

void test_modulec_run_moduleb_enabled_get_is_null(void)
{
    moduleB_isEnabled_ExpectAndReturn(true);
    moduleB_get_ExpectAndReturn(NULL);
    TEST_ASSERT_FALSE(modulec_run());
}

void test_modulec_run_moduleb_enabled_get_returns_99(void)
{
    moduleB_data data;
    data.value = 99;

    moduleB_isEnabled_ExpectAndReturn(true);
    moduleB_get_ExpectAndReturn(&data);
    moduleB_set_Expect(55);
    TEST_ASSERT_FALSE(modulec_run());
}

void test_modulec_run_moduleb_enabled_get_returns_100(void)
{
    moduleB_data data;
    data.value = 100;

    moduleB_isEnabled_ExpectAndReturn(true);
    moduleB_get_ExpectAndReturn(&data);
    TEST_ASSERT_TRUE(modulec_run());
}
